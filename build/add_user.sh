#!/bin/bash
set -e
source /bd_build/buildconfig
set -x

$minimal_apt_get_install sudo

# Workaround for sudo errors in containers, see: https://github.com/sudo-project/sudo/issues/42
echo "Set disable_coredump false" >> /etc/sudo.conf

mkdir -p /var/shwoncast/servers/shoutcast2 /var/shwoncast/stations /var/shwoncast/www_tmp 

adduser --home /var/shwoncast --disabled-password --gecos "" shwoncast

chown -R shwoncast:shwoncast /var/shwoncast

echo 'shwoncast ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers